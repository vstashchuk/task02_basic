package com.epam;

/**
 * Build Fibonacci class.
 */
public final class Fibonacci {
    /**
     * Constructor.
     */
    private Fibonacci() {

    }

    /**
     * get N number of Fibonacci.
     * @param n - th Fibonacci number.
     * @return sum of previous Fibonacci numbers.
     */
    static int getFibNumber(final int n) {
        if (n <= 1) {
            return n;
        }
        return getFibNumber(n - 1) + getFibNumber(n - 2);
    }
}
