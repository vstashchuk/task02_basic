/**
 * com.epam is a group of bar utils for operating on odd and even numbers.
 * also build Fibonacci numbers.
 */
package com.epam;

import java.io.IOException;
import java.util.Scanner;

/**
 * Class for making all functions with numbers and interaction with user.
 */
final class Application {
    /**
     * Constructor.
     */
    private Application() {

    }

    /**
     * Entry point into the program.
     *
     * @param args args for main
     * @throws IOException if there are the issues.
     */
    public static void main(final String[] args) throws IOException {
        final int nPercentage = 100;
        System.out.println("Enter range from A to B: ");
        Scanner scanner = new Scanner(System.in);
        int nA = scanner.nextInt();
        int nB = scanner.nextInt();
        int sumOdd = 0;
        int sumEven = 0;
        for (int i = nA; i <= nB; i++) {
            if (i % 2 == 0) {
                sumEven += i;
                System.out.println(i);
            }
        }

        for (int i = nB; i >= nA; i--) {
            if (i % 2 != 0) {
                sumOdd += i;
                System.out.println(i);
            }
        }

        System.out.println("Sum of odd numbers " + sumOdd);
        System.out.println("Sum of even numbers " + sumEven);
        System.out.println("Enter number of set : ");
        int nSetNumber = scanner.nextInt();
        int nBiggestOdd = 0;
        int nBiggestEven = 0;
        int numOfOdd = 0, numOfEven = 0;
        for (int i = 0; i < nSetNumber; i++) {
            int nCurFibNumber = Fibonacci.getFibNumber(i);
            if (nCurFibNumber % 2 != 0) {
                nBiggestOdd = nCurFibNumber;
                numOfOdd++;
            }
            if (nCurFibNumber % 2 == 0) {
                nBiggestEven = nCurFibNumber;
                numOfEven++;
            }
        }
        System.out.println("The biggest odd number :" + nBiggestOdd);
        System.out.println("The biggest even number "
                + ": " + nBiggestEven);
        System.out.println("Percentage of odd numbers : "
                + (numOfOdd / (nSetNumber * 1.0)) * nPercentage);
        System.out.println("Percentage of even numbers : "
                + (numOfEven / (nSetNumber * 1.0)) * nPercentage);
    }
}
